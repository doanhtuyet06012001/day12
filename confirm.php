<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirm</title>
    <link rel="stylesheet" href="./css/styles.css">
</head>

<body>
    <?php
    $gender = 0;
    $group = '';
    session_start();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        try {
            $conn = new mysqli("10.0.1.2", "root", "my-secret-pw", "mydb");
            if ($conn->connect_errno) {
                echo "<script>console.log(\"" . $conn->connect_error . "\")</script>";
                exit();
            };

            $conn->query("CREATE TABLE IF NOT EXISTS `student` (
            `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `name` varchar(250) NOT NULL,
            `gender` int(1) NOT NULL,
            `faculty` char(3) NOT NULL,
            `birthday` date NOT NULL,
            `address` varchar(250) DEFAULT NULL,
            `avatar` text DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

            if (isset($_SESSION["gender"]) and $_SESSION["gender"] === "Nam") {
                $gender = 1;
            } else {
                $gender = 0;
            }

            if (isset($_SESSION["group"]) and $_SESSION["group"] === "Khoa học máy tính") {
                $group = "MAT";
            } else {
                $group = "KDL";
            }

            $stmt = $conn->prepare("INSERT INTO 
            `student` (`name`, `gender`, `faculty`, `birthday`, `address`, `avatar`) 
            VALUES 
            (?, ?, ?, ?, ?, ?);");
            $stmt->bind_param("sissss", $_SESSION["name"], $gender, $group, date('Y-m-d', strtotime(str_replace('/', '-', $_SESSION["birthday"]))), $_SESSION["address"], $_SESSION["image"]);
            $stmt->execute();
            $stmt->close();
            $conn->close();
            header("Location: success.php");
        } catch (Exception $e) {
            echo "<script>alert(\"$e\")</script>";
            exit();
        };
    }
    ?>
    <form class="container" method="POST">
        <div class="row">
            <div class="col1 label required">Họ và tên</div>
            <div class="col2">
                <?php
                $name = $_SESSION["name"];
                echo "<div style=\"margin: auto 0;\">$name</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Giới tính</div>
            <div class="col2">
                <?php session_start();
                $gender = $_SESSION["gender"];
                echo "<div style=\"margin: auto 0;\">$gender</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Phân khoa</div>
            <div class="col2">
                <?php session_start();
                $group = $_SESSION["group"];
                echo "<div style=\"margin: auto 0;\">$group</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Ngày sinh</div>
            <div class="col2">
                <?php session_start();
                $birthday = $_SESSION["birthday"];
                echo "<div style=\"margin: auto 0;\">$birthday</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label">Địa chỉ</div>
            <div class="col2">
                <?php session_start();
                $address = $_SESSION["address"];
                echo "<div style=\"margin: auto 0;\">$address</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label">Hình ảnh</div>
            <div class="col2">
                <?php session_start();
                $image = $_SESSION["image"];
                if ($image !== "") {
                    echo "<img src=\"$image\" alt=\"\" style=\"width: 100px; height: 100px;\">";
                } ?>
            </div>
        </div>
        <div class="row" style="justify-content: center; margin: 25px 0 0 0;">
            <button type="submit" style="padding: 16px 56px;">Xác nhận</button>
        </div>
    </form>
</body>

</html>