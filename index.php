<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <style>
        .cborder {
            border: 1px solid blue;
            border-radius: 0;
            box-sizing: border-box;
            background-color: #e1eaf4;
        }

        .cbutton {
            border: 1px solid blue;
            border-radius: 15px;
            box-sizing: border-box;
            background-color: #4f81bd;
            font-size: 20px;
            color: white;
        }

        .subborder {
            border: 1px solid blue;
            border-radius: 0;
            box-sizing: border-box;
            background-color: #8baacf;
        }
    </style>
</head>

<body>
    <div class="container">
        <form method="post">
            <?php
            session_start();
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $_SESSION['group'] = $_POST['group'];
                $_SESSION['keywords'] = $_POST['keywords'];
            }
            ?>
            <div class="mx-auto py-5" style="width: 50%">
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label" for="group">Khoa</label>
                    <div class="col-sm-10">
                        <select class="form-control cborder" name="group" id="group">
                            <?php
                            session_start();
                            $DSKHOA = array(
                                null => "",
                                "MAT" => "Khoa học máy tính",
                                "KDL" => "Khoa học vật liệu"
                            );
                            foreach ($DSKHOA as $key => $khoa) {
                                if ($key == $_SESSION["group"]) {
                                    echo "<option value='$key' selected>$khoa</option>";
                                } else {
                                    echo "<option value='$key'>$khoa</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label class="col-sm-2 col-form-label" for="keywords">Từ khóa</label>
                    <div class="col-sm-10">
                        <input id="keywords" name="keywords" class="form-control cborder" type="text" placeholder="" <?php echo "value=\"" . $_SESSION['keywords'] . "\""; ?>>
                    </div>
                </div>
                <div class="text-center">
                    <button id="resetInput" class="btn btn-primary cbutton">Xóa</button>
                    <button id="search" type="submit" class="btn btn-primary cbutton">Tìm kiếm</button>
                </div>
                <script>
                    document.getElementById("resetInput").onclick = function() {
                        document.getElementById("group").value = "";
                        document.getElementById("keywords").value = "";
                    };
                </script>
        </form>
        <div class="py-3">Số sinh viên tìm thấy: XXX</div>
        <div class="d-flex flex-row-reverse">
            <button id="newItem" type="submit" class="btn btn-primary cbutton" style="font-size: 16px;"><a href="register.php">Thêm</a></button>
        </div>
    </div>
    <div class="mx-auto" style="width:75%">
        <div class="row">
            <div class="col-1">No</div>
            <div class="col-3">Tên sinh viên</div>
            <div class="col-6">Khoa</div>
            <div class="col-2 text-center">Action</div>
        </div>
        <?php
        $RESULT = array(
            array(1, "Nguyễn Văn A", "Khoa học máy tính"),
            array(2, "Trần Thị B", "Khoa học máy tính"),
            array(3, "Nguyễn Hoàng C", "Khoa học vật liệu"),
            array(4, "Đinh Quang D", "Khoa học vật liệu"),
        );
        foreach ($RESULT as $student) {
            echo "
                <div class=\"row py-1\">
                    <div class=\"col-1\">$student[0]</div>
                    <div class=\"col-3\">$student[1]</div>
                    <div class=\"col-6\">$student[2]</div>
                    <div class=\"col-2 text-center\">
                        <button class=\"subborder\" style=\"color: white;\">Xóa</button>
                        <button class=\"subborder\" style=\"color: white;\">Sửa</button>
                    </div>
                </div>
                ";
        }
        ?>
    </div>
    </div>
</body>

</html>