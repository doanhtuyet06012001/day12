<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="./css/styles.css">
</head>

<body>
    <form class="container" method="post" enctype="multipart/form-data">
        <?php
        session_start();
        $name = $gender = $group = $birthday = $target_file = "";
        $valid = true;

        function on_error($message)
        {
            echo "<div class=\"row\" style=\"color: red\">$message</div>";
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (empty($_POST["fullname"])) {
                on_error("Hãy nhập tên");
                $valid = false;
            } else {
                $name = $_POST["fullname"];
            }
            if (!isset($_POST["gender"])) {
                on_error("Hãy chọn giới tính");
                $valid = false;
            } else {
                $gender = $_POST["gender"];
            }
            if (empty($_POST["group"])) {
                on_error("Hãy chọn phân khoa");
                $valid = false;
            } else {
                $group = $_POST["group"];
            }
            if (empty($_POST["birthday"])) {
                on_error("Hãy nhập ngày sinh");
                $valid = false;
            } else {
                if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/", $_POST["birthday"])) {
                    $date = explode("/", $_POST["birthday"]);
                    if (!checkdate($date[1], $date[0], $date[2])) {
                        on_error("Hãy nhập ngày sinh đúng định dạng");
                        $valid = false;
                    } else {
                        $birthday = $_POST["birthday"];
                    }
                } else {
                    on_error("Hãy nhập ngày sinh đúng định dạng" . $date);
                    $valid = false;
                }
            }

            if ($_FILES["fileToUpload"]["size"] > 0) {
                $target_dir = "uploads/";
                $uploaded = pathinfo($_FILES["fileToUpload"]["name"]);
                $target_file = $target_dir . $uploaded['filename'] . "_" . date("YmdHis") . "." . $uploaded['extension'];
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

                // Create uploads folder if not exists
                if (!is_dir($target_dir)) {
                    if (!@mkdir($target_dir, 0777, true)) {
                        $error = error_get_last();
                        on_error($error['message']);
                        $valid = false;
                    }
                }

                if ($check !== false) {
                    $uploadOk = 1;
                } else {
                    on_error($_FILES["fileToUpload"]["type"] . " is not an image.");
                    $valid = false;
                    $uploadOk = 0;
                }

                if ($uploadOk != 0 && $valid) {
                    if (@move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        echo "<script>console.log(\"The file " . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " has been uploaded.\")</script>";
                    } else {
                        on_error("Sorry, there was an error uploading your file.");
                        $valid = false;
                    }
                }
            }

            if ($valid) {
                $_SESSION["name"] = $name;
                $_SESSION["gender"] = $gender;
                $_SESSION["group"] = $group;
                $_SESSION["birthday"] = $birthday;
                $_SESSION["address"] = $_POST["address"];
                $_SESSION["image"] = $target_file;
                header("Location: ./confirm.php");
                exit();
            }
        }
        ?>

        <div class="row">
            <div class="col1 label required">Họ và tên</div>
            <div class="col2"><input style="display: block; flex: 1; border: 2px solid blue;" type="text" name="fullname"></div>
        </div>

        <div class="row">
            <div class="col1 label required">Giới tính</div>
            <div class="col2">
                <?php
                $gender = array(
                    0 => "Nam",
                    1 => "Nữ"
                );
                for ($i = 0; $i < count($gender); $i++) {
                    echo "<div style=\"margin: auto 0;\">
                        <input style=\"accent-color: green;\" type=\"radio\" name=\"gender\" value=\"$gender[$i]\" id=\"$gender[$i]\">
                            <label for=\"$gender[$i]\">$gender[$i]</label>
                        </div>
                        ";
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col1 label required">Phân khoa</div>
            <div class="col2">
                <select style="flex: 1," name="group">
                    <?php
                    $DSKHOA = array(
                        null => "",
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    );
                    foreach ($DSKHOA as $key => $khoa) {
                        echo "<option>$khoa</option>";
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col1 label required">Ngày sinh</div>
            <div class="col2">
                <input placeholder="dd/mm/yyyy" style="border: 2px solid blue; width: auto," name="birthday">
            </div>
        </div>

        <div class="row">
            <div class="col1 label">Địa chỉ</div>
            <div class="col2"><input style="display: block; flex: 1; border: 2px solid blue;" type="text" name="address"></div>
        </div>

        <div class="row">
            <div class="col1 label">Hình ảnh</div>
            <div class="col2"><input style="margin: auto 0;" type="file" name="fileToUpload" id="fileToUpload"></div>
        </div>

        <div class="row" style="justify-content: center; margin: 25px 0 0 0;">
            <button style="padding: 16px 56px;">Đăng ký</button>
        </div>
    </form>
</body>

</html>